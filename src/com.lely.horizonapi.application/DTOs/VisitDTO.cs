﻿using com.lely.horizonapi.domain.entities;
using System.Text.Json.Serialization;

namespace com.lely.horizonapi.application.DTOs
{
	public class VisitDTO
	{
		[JsonPropertyName("DeviceId")]
		public Guid DeviceId { get; set; }

		[JsonPropertyName("AnimalId")]
		public Guid AnimalId { get; set; }

		[JsonPropertyName("Animal")]
		public Animal? Animal { get; set; }

		[JsonPropertyName("FeedingDate")]
		public DateTime FeedingDate { get; set; }

		[JsonPropertyName("Intake")]
		public int? Intake { get; set; }

		public VisitDTO(Visit visit, Animal? animal)
		{
			this.DeviceId = visit.DeviceId;
			this.AnimalId = visit.AnimalId;
			this.FeedingDate = visit.FeedingDate;
			this.Intake = visit.Intake;
			this.Animal = animal;
		}

		public override bool Equals(object? obj)
		{
			if(obj == null)
				return false;

			try
			{
				if (this.Animal != null && !this.Animal.Equals(((VisitDTO)obj).Animal))
					return false;
				else if (this.Animal == null && ((VisitDTO)obj).Animal != null)
					return false;

				return this.DeviceId.Equals(((VisitDTO)obj).DeviceId) &&
					this.AnimalId.Equals(((VisitDTO)obj).AnimalId) &&
					this.FeedingDate.Equals(((VisitDTO)obj).FeedingDate) &&
					this.Intake.Equals(((VisitDTO)obj).Intake);
			} catch
			{
				return false;
			}
		}
	}
}
