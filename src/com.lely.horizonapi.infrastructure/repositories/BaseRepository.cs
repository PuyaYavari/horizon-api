﻿using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Linq.Expressions;
using com.lely.horizonapi.domain.repositories;

namespace com.lely.horizonapi.infrastructure.repositories
{
	public class BaseRepository<Entity> : IRepository<Entity> where Entity : class
	{
        internal HorizonContext context;
        internal DbSet<Entity> dbSet;

        public BaseRepository(HorizonContext context)
        {
            this.context = context;
            this.dbSet = context.Set<Entity>();
        }

        public async virtual Task<int> Save()
        {
            return await context.SaveChangesAsync();
        }

        public async virtual Task<IEnumerable<Entity>> Get(
            Expression<Func<Entity, bool>>? filter = null,
            Func<IQueryable<Entity>, IOrderedQueryable<Entity>>? orderBy = null,
            string includeProperties = "",
            int pageSize = 50,
            int page = 1
        )
        {
            if (page < 1)
                throw new ArgumentOutOfRangeException(nameof(page), "Page cannot be smaller than 1.");

            IQueryable<Entity> query = dbSet;
            if (filter != null)
            {
                query = query.Where(filter);
                Log.Debug("Filter added to query.");
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
                Log.Debug($"{includeProperty} to query.");
            }

            if (orderBy != null)
            {
                Log.Debug("Ordered result is being returned.");
                return await orderBy(query).Skip(pageSize * (page - 1)).Take(pageSize).ToListAsync();
            }
            else
            {
                Log.Debug("Result is being returned.");
                return await query.Skip(pageSize * (page - 1)).Take(pageSize).ToListAsync();
            }
        }

        public virtual async Task<Entity?> GetByID(
            object id
        )
        {
            Log.Debug($"Entity with id {id} is being returned.");
            return await dbSet.FindAsync(id);
        }

        public async virtual Task<Entity> Insert(Entity entity)
        {
            Log.Debug($"Entity is being inserted.");
            var inserted = dbSet.Add(entity).Entity;
            await Save();
            return inserted;
        }

        public async virtual void Delete(object id)
        {
            Log.Debug($"Entity with id {id} is being deleted.");
            Entity? entityToDelete = await GetByID(id);
            if(entityToDelete != null)
			{
                Delete(entityToDelete);
                await Save();
			}
        }

        public async virtual void Delete(Entity entityToDelete)
        {
            Log.Debug($"Entity is being deleted.");
            if (context.Entry(entityToDelete).State == EntityState.Detached)
                dbSet.Attach(entityToDelete);
            dbSet.Remove(entityToDelete);
            await Save();
        }

        public async virtual Task<Entity> Update(Entity entityToUpdate)
        {
            Log.Debug($"Entity is being updated.");
            Entity updated = dbSet.Attach(entityToUpdate).Entity;
            context.Entry(entityToUpdate).State = EntityState.Modified;
            await Save();
            return updated;
        }
    }
}
