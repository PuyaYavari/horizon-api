﻿using com.lely.horizonapi.domain.entities;
using Microsoft.EntityFrameworkCore;

namespace com.lely.horizonapi.infrastructure
{
	public class HorizonContext : DbContext
	{
		private readonly string DbPath;

		public DbSet<Animal>? Animals { get; set; }
		public DbSet<Visit>? Visits { get; set; }

		public HorizonContext()
		{
			DbPath = $"{AppDomain.CurrentDomain.BaseDirectory}Horizon.db";
			Database.EnsureCreated();
		}

		protected override void OnConfiguring(DbContextOptionsBuilder options)
			=> options.UseSqlite($"Data Source={DbPath}");
	}
}
