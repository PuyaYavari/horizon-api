﻿using com.lely.horizonapi.domain.enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace com.lely.horizonapi.domain.entities
{
	[Table("Animals")]
	public class Animal
	{
		/// <summary>
		/// Animals Unique Identifier
		/// </summary>
		[Key]
		[Column("ID")]
		[JsonPropertyName("Id")]
		public Guid Id { get; set; }

		/// <summary>
		/// Animal Life Number
		/// </summary>
		[Column("LIFE_NUMBER")]
		[JsonPropertyName("LifeNumber")]
		public string? LifeNumber { get; set; }

		/// <summary>
		/// Animals Name
		/// </summary>
		[MaxLength(64)]
		[Column("NAME")]
		[JsonPropertyName("Name")]
		public string? Name { get; set; }

		/// <summary>
		/// Animals Gender
		/// </summary>
		[Column("GENDER")]
		[JsonPropertyName("Gender")]
		public AnimalGender Gender { get; set; }

		/// <summary>
		/// Animals Date Of Birth
		/// </summary>
		[Column("BIRTH_DATE")]
		[JsonPropertyName("BirthDate")]
		public DateTime BirthDate { get; set; }

		/// <summary>
		/// Animals Fathers Life Name
		/// </summary>
		[Column("FATHER_LIFE_NUMBER")]
		[JsonPropertyName("FatherLifeNumber")]
		public string? FatherLifeNumber { get; set; }

		/// <summary>
		/// Animals Mothers Life Name
		/// </summary>
		[Column("MOTHER_LIFE_NUMBER")]
		[JsonPropertyName("MotherLifeNumber")]
		public string? MotherLifeNumber { get; set; }

		/// <summary>
		/// Description About The Animal
		/// </summary>
		[MaxLength(1024)]
		[Column("DESCRIPTION")]
		[JsonPropertyName("Description")]
		public string? Description { get; set; }
	}
}
