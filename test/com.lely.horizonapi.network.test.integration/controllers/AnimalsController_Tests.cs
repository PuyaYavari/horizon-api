﻿using Bogus;
using com.lely.horizonapi.application.services;
using com.lely.horizonapi.domain.entities;
using com.lely.horizonapi.domain.enums;
using com.lely.horizonapi.domain.repositories;
using com.lely.horizonapi.network.controllers;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xunit;

namespace com.lely.horizonapi.network.test.integration.controllers
{
	public class AnimalsController_Tests
	{

		[Fact]
		public async Task GetAllTest()
		{
			IEnumerable<Animal> animals = new List<Animal>();
			for(int i = 0; i < 10; i++)
			{
				animals = animals.Append(GenerateMockAnimal());
			}
			animals = animals.OrderBy(a => a.BirthDate).ToList();
			Mock<IRepository<Animal>> repository = new();
			repository.Setup(c => c.Get(
					It.IsAny<Expression<Func<Animal, bool>>?>(), 
					It.IsAny<Func<IQueryable<Animal>, IOrderedQueryable<Animal>>?>(), 
					It.IsAny<string>(), 
					It.IsAny<int>(), 
					It.IsAny<int>())
				).Returns(Task.FromResult(animals));
			AnimalsService service = new(repository.Object);
			AnimalsController controller = new(service);
			Assert.Equal(animals, await controller.GetAll(null, null, null, null, null, null, null));
		}

		[Fact]
		public async Task GetTest()
		{
			Animal mockAnimal = GenerateMockAnimal();
			Mock<IRepository<Animal>> repository = new();
			repository.Setup(c => c.GetByID(mockAnimal.Id)).Returns(Task.FromResult<Animal?>(mockAnimal));
			AnimalsService service = new(repository.Object);
			AnimalsController controller = new(service);
			Assert.Equal(mockAnimal, await controller.Get(mockAnimal.Id));
		}

		[Fact]
		public async Task InsertTest()
		{
			Animal mockAnimal = GenerateMockAnimal();
			Mock<IRepository<Animal>> repository = new();
			repository.Setup(c => c.Insert(mockAnimal)).Returns(Task.FromResult(mockAnimal));
			AnimalsService service = new(repository.Object);
			AnimalsController controller = new(service);
			Assert.Equal(mockAnimal, await controller.Insert(mockAnimal));
		}

		[Fact]
		public async Task UpdateTest()
		{
			Animal mockAnimal = GenerateMockAnimal();
			Mock<IRepository<Animal>> repository = new();
			repository.Setup(c => c.Update(mockAnimal)).Returns(Task.FromResult(mockAnimal));
			AnimalsService service = new(repository.Object);
			AnimalsController controller = new(service);
			Assert.Equal(mockAnimal, await controller.Update(mockAnimal.Id, mockAnimal));
		}

		private static Animal GenerateMockAnimal() => new Faker<Animal>()
				.RuleFor(c => c.Id, Guid.NewGuid())
				.RuleFor(c => c.Name, f => f.Name.FirstName())
				.RuleFor(c => c.LifeNumber, f => f.Random.AlphaNumeric(8))
				.RuleFor(c => c.MotherLifeNumber, f => f.Random.AlphaNumeric(8))
				.RuleFor(c => c.FatherLifeNumber, f => f.Random.AlphaNumeric(8))
				.RuleFor(c => c.BirthDate, f => f.Date.Recent(0))
				.RuleFor(c => c.Gender, f => f.PickRandom<AnimalGender>())
				.RuleFor(c => c.Description, f => f.Lorem.Sentence(5));
	}
}
