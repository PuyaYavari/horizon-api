﻿using com.lely.horizonapi.domain.decorators;

namespace com.lely.horizonapi.domain.enums
{
	[JsonStringEnum]
	public enum AnimalGender
	{
		Male,
		Female
	}
}
