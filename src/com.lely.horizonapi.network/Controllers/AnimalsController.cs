﻿using com.lely.horizonapi.application.DTOs;
using com.lely.horizonapi.application.services.Interfaces;
using com.lely.horizonapi.domain.entities;
using com.lely.horizonapi.domain.enums;
using Microsoft.AspNetCore.Mvc;

namespace com.lely.horizonapi.network.controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class AnimalsController : ControllerBase
	{
		private readonly IAnimalsService _animalService;

		public AnimalsController(IAnimalsService animalService)
		{
			this._animalService = animalService;
		}

		/// <summary>
		/// Get all the animals with the specified criteria, page by page.
		/// </summary>
		/// <param name="LifeNumber">LifeNumber Of The Animal To Get</param>
		/// <param name="Name">Name Of The Animal To Get</param>
		/// <param name="Gender">Gender Of The Animal To Get</param>
		/// <param name="BirthDate">BirthDate Of The Animal To Get</param>
		/// <param name="FatherLifeNumber">FatherLifeNumber Of The Animal To Get</param>
		/// <param name="MotherLifeNumber">MotherLifeNumber Of The Animal To Get</param>
		/// <param name="Description">Description Of The Animal To Get</param>
		/// <param name="PageNumber">Page To Get</param>
		/// <param name="PageSize">The Number Of Animals In Each Page</param>
		/// <returns>
		/// The animals with the given criteria.
		/// </returns>
		/// <response code="200">Animals with the given criteria</response>
		/// <response code="400">PageNumber less than 1 or PageSize larger than 100</response>
		[HttpGet]
		[ProducesResponseType(typeof(IEnumerable<Animal>), 200)]
		[ProducesResponseType(typeof(ErrorDTO), 400)]
		[ProducesResponseType(500)]
		public async Task<object> GetAll(
			[FromQuery] string? LifeNumber,
			[FromQuery] string? Name,
			[FromQuery] AnimalGender? Gender,
			[FromQuery] DateTime? BirthDate,
			[FromQuery] string? FatherLifeNumber,
			[FromQuery] string? MotherLifeNumber,
			[FromQuery] string? Description,
			[FromQuery] int PageNumber = 1,
			[FromQuery] int PageSize = 50
		)
		{
			if (PageNumber < 1)
				return BadRequest("PageNumber cannot be smaller than 1.");

			if (PageSize > 100)
				return BadRequest("PageSize cannot be larger than 100.");
				
			return await this._animalService.GetAnimals(
				LifeNumber,
				Name,
				Gender,
				BirthDate,
				FatherLifeNumber,
				MotherLifeNumber,
				Description, 
				PageNumber, 
				PageSize
			);
		}

		/// <summary>
		/// Get the animal with the given <paramref name="id"/>.
		/// </summary>
		/// <param name="id">Identifier Of The Animal To Get</param>
		/// <returns>
		/// The animal if the animal with the given id exists.
		/// </returns>
		/// <response code="200">Animal with the given id</response>
		/// <response code="404">Animal with the given id does not exist</response>
		[HttpGet("{id}")]
		[ProducesResponseType(typeof(Animal),200)]
		[ProducesResponseType(typeof(ErrorDTO), 404)]
		[ProducesResponseType(500)]
		public async Task<object> Get([FromRoute] Guid id)
		{
			Animal? animal = await this._animalService.GetAnimal(id);
			if (animal != null)
				return animal;
			else
				return NotFound(new ErrorDTO($"Animal with given id is not found. Id: {id}" ));
		}

		/// <summary>
		/// Inserts <paramref name="animal"/> into the databse.
		/// </summary>
		/// <param name="animal">Animal To Insert</param>
		/// <returns>Inserted Animal</returns>
		/// <response code="200">Inserted Animal</response>
		[HttpPost]
		[ProducesResponseType(typeof(Animal), 200)]
		[ProducesResponseType(500)]
		public async Task<Animal> Insert([FromBody] Animal animal)
		{
			return await this._animalService.InsertAnimal(animal);
		}

		/// <summary>
		/// Updates animal with identifier <paramref name="id"/>, based on <paramref name="animal"/>.
		/// </summary>
		/// <param name="id">Identifier Of The Animal To Update</param>
		/// <param name="animal">Animals New Value</param>
		/// <returns>Updated Animal</returns>
		/// <response code="200">Updated Animal</response>
		[HttpPut("{id}")]
		[ProducesResponseType(typeof(Animal), 200)]
		[ProducesResponseType(500)]
		public async Task<Animal> Update([FromRoute] Guid id, [FromBody] Animal animal)
		{
			return await this._animalService.UpdateAnimal(id, animal);
		}

		/// <summary>
		/// Deletes the animal with the identifier <paramref name="id"/> from the database.
		/// </summary>
		/// <param name="id">Identifier Of The Animal To Delete</param>
		[HttpDelete("{id}")]
		[ProducesResponseType(204)]
		public IActionResult Delete([FromRoute] Guid id)
		{
			this._animalService.DeleteAnimal(id);
			return NoContent();
		}
	}
}
