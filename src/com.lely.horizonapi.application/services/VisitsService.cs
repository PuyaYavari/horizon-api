﻿using com.lely.horizonapi.application.DTOs;
using com.lely.horizonapi.application.services.Interfaces;
using com.lely.horizonapi.application.utils;
using com.lely.horizonapi.domain.entities;
using com.lely.horizonapi.domain.repositories;
using System.Linq.Expressions;

namespace com.lely.horizonapi.application.services
{
	public class VisitsService : IVisitsService
	{
		private readonly IRepository<Visit> _visitRepository;
		public readonly IAnimalsService _animalsService;

		public VisitsService(IRepository<Visit> visitRepository, IAnimalsService animalsService)
		{
			this._visitRepository = visitRepository;
			this._animalsService = animalsService;
		}

		public async Task<IEnumerable<VisitDTO>> GetVisits(Guid? AnimalId, DateTime? FeedingDate, int? Intake, int PageNumber, int PageSize)
		{
			Expression<Func<Visit, bool>> filter = a => true;

			if (AnimalId != null)
			{
				Expression<Func<Visit, bool>> expression = v => v.AnimalId == AnimalId;
				filter = Util.CombineExpressionsWithAndAlso(filter, expression);
			}

			if (FeedingDate != null)
			{
				Expression<Func<Visit, bool>> expression = v => v.FeedingDate == FeedingDate;
				filter = Util.CombineExpressionsWithAndAlso(filter, expression);
			}

			if (Intake != null)
			{
				Expression<Func<Visit, bool>> expression = v => v.Intake == Intake;
				filter = Util.CombineExpressionsWithAndAlso(filter, expression);
			}

			IEnumerable<Visit> visits = await this._visitRepository.Get(filter, q => q.OrderBy(v => v.FeedingDate), pageSize: PageSize, page: PageNumber);

			IEnumerable<VisitDTO> output = new List<VisitDTO>();
			foreach (Visit visit in visits)
			{
				output = output.Append(new VisitDTO(visit, await this._animalsService.GetAnimal(visit.AnimalId)));
			}

			return output;
		}

		public async Task<VisitDTO?> GetVisit(Guid id)
		{
			Visit? visit = await this._visitRepository.GetByID(id);
			if (visit == null)
				return null;
			return new VisitDTO(visit, await this._animalsService.GetAnimal(visit.AnimalId));
		}

		public async Task<VisitDTO> InsertVisit(Visit visit)
		{
			Visit insertedVisit = await this._visitRepository.Insert(visit);
			return new VisitDTO(insertedVisit, await this._animalsService.GetAnimal(insertedVisit.AnimalId));
		}

		public async Task<VisitDTO> UpdateVisit(Guid id, Visit visit)
		{
			visit.DeviceId = id;
			Visit updatedVisit = await this._visitRepository.Update(visit);
			return new VisitDTO(updatedVisit, await this._animalsService.GetAnimal(updatedVisit.AnimalId));
		}

		public void DeleteVisit(Guid id) => this._visitRepository.Delete(id);
	}
}
