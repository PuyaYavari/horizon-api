﻿using Bogus;
using com.lely.horizonapi.application.DTOs;
using com.lely.horizonapi.application.services;
using com.lely.horizonapi.application.services.Interfaces;
using com.lely.horizonapi.domain.entities;
using com.lely.horizonapi.domain.enums;
using com.lely.horizonapi.domain.repositories;
using com.lely.horizonapi.network.controllers;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xunit;

namespace com.lely.horizonapi.network.test.integration.controllers
{
	public class VisitsController_Tests
	{

		[Fact]
		public async Task GetTest()
		{
			Animal mockAnimal = GenerateMockAnimal();
			Visit mockVisit = GenerateMockVisit(mockAnimal.Id);
			VisitDTO mockVisitDto = new(mockVisit, mockAnimal);

			Mock<IRepository<Visit>> repository = new();
			repository.Setup(c => c.GetByID(mockVisit.DeviceId)).Returns(Task.FromResult<Visit?>(mockVisit));

			Mock<IAnimalsService> animalsService = new();
			animalsService.Setup(c => c.GetAnimal(mockAnimal.Id)).Returns(Task.FromResult<Animal?>(mockAnimal));

			VisitsService service = new(repository.Object, animalsService.Object);
			VisitsController controller = new(service);

			Assert.Equal(mockVisitDto, await controller.Get(mockVisit.DeviceId));
		}

		[Fact]
		public async Task GetAllTest()
		{
			IEnumerable<VisitDTO> visitDtos = new List<VisitDTO>();
			IEnumerable<Visit> visits = new List<Visit>();
			IEnumerable<Animal> animals = new List<Animal>();

			for (int i = 0; i < 10; i++)
			{
				Animal mockAnimal = GenerateMockAnimal();
				Visit mockVisit = GenerateMockVisit(mockAnimal.Id);
				animals = animals.Append(mockAnimal);
				visits = visits.Append(mockVisit);
				visitDtos = visitDtos.Append(new(mockVisit, mockAnimal));
			}

			visits = visits.OrderBy(x => x.FeedingDate).ToList();
			visitDtos = visitDtos.OrderBy(x => x.FeedingDate).ToList();

			Mock<IRepository<Visit>> repository = new();
			repository.Setup(c => c.Get(
					It.IsAny<Expression<Func<Visit, bool>>?>(),
					It.IsAny<Func<IQueryable<Visit>, IOrderedQueryable<Visit>>?>(),
					It.IsAny<string>(),
					It.IsAny<int>(),
					It.IsAny<int>())
				).Returns(Task.FromResult(visits));

			Mock<IAnimalsService> animalsService = new();
			foreach(Animal animal in animals)
				animalsService.Setup(c => c.GetAnimal(animal.Id)).Returns(Task.FromResult<Animal?>(animal));

			VisitsService service = new(repository.Object, animalsService.Object);
			VisitsController controller = new(service);

			Assert.Equal(visitDtos, await controller.GetAll(null, null, null));
		}

		[Fact]
		public async Task InsertTest()
		{
			Animal mockAnimal = GenerateMockAnimal();
			Visit mockVisit = GenerateMockVisit(mockAnimal.Id);
			VisitDTO mockVisitDto = new(mockVisit, mockAnimal);

			Mock<IRepository<Visit>> repository = new();
			repository.Setup(c => c.Insert(mockVisit)).Returns(Task.FromResult<Visit>(mockVisit));

			Mock<IAnimalsService> animalsService = new();
			animalsService.Setup(c => c.GetAnimal(mockAnimal.Id)).Returns(Task.FromResult<Animal?>(mockAnimal));

			VisitsService service = new(repository.Object, animalsService.Object);
			VisitsController controller = new(service);

			Assert.Equal(mockVisitDto, await controller.Insert(mockVisit));
		}

		[Fact]
		public async Task UpdateTest()
		{
			Animal mockAnimal = GenerateMockAnimal();
			Visit mockVisit = GenerateMockVisit(mockAnimal.Id);
			VisitDTO mockVisitDto = new(mockVisit, mockAnimal);

			Mock<IRepository<Visit>> repository = new();
			repository.Setup(c => c.Update(mockVisit)).Returns(Task.FromResult<Visit>(mockVisit));

			Mock<IAnimalsService> animalsService = new();
			animalsService.Setup(c => c.GetAnimal(mockAnimal.Id)).Returns(Task.FromResult<Animal?>(mockAnimal));

			VisitsService service = new(repository.Object, animalsService.Object);
			VisitsController controller = new(service);

			Assert.Equal(mockVisitDto, await controller.Update(mockVisit.DeviceId, mockVisit));
		}

		private static Visit GenerateMockVisit(Guid animalId) => new Faker<Visit>()
			.RuleFor(c => c.DeviceId, Guid.NewGuid)
			.RuleFor(c => c.AnimalId, animalId)
			.RuleFor(c => c.FeedingDate, f => f.Date.Recent(0))
			.RuleFor(c => c.Intake, f => f.Random.Int(1,100));

		private static Animal GenerateMockAnimal() => new Faker<Animal>()
			.RuleFor(c => c.Id, Guid.NewGuid())
			.RuleFor(c => c.Name, f => f.Name.FirstName())
			.RuleFor(c => c.LifeNumber, f => f.Random.AlphaNumeric(8))
			.RuleFor(c => c.MotherLifeNumber, f => f.Random.AlphaNumeric(8))
			.RuleFor(c => c.FatherLifeNumber, f => f.Random.AlphaNumeric(8))
			.RuleFor(c => c.BirthDate, f => f.Date.Recent(0))
			.RuleFor(c => c.Gender, f => f.PickRandom<AnimalGender>())
			.RuleFor(c => c.Description, f => f.Lorem.Sentence(5));
	}
}
