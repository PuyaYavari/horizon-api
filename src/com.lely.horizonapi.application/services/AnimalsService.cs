﻿using com.lely.horizonapi.application.services.Interfaces;
using com.lely.horizonapi.application.utils;
using com.lely.horizonapi.domain.entities;
using com.lely.horizonapi.domain.enums;
using com.lely.horizonapi.domain.repositories;
using System.Linq.Expressions;

namespace com.lely.horizonapi.application.services
{
	public class AnimalsService : IAnimalsService
	{
		private readonly IRepository<Animal> _animalRepository;

		public AnimalsService(IRepository<Animal> animalRepository)
		{
			this._animalRepository = animalRepository;
		}


		public async Task<IEnumerable<Animal>> GetAnimals(
			string? LifeNumber,
			string? Name,
			AnimalGender? Gender,
			DateTime? BirthDate,
			string? FatherLifeNumber,
			string? MotherLifeNumber,
			string? Description,
			int PageNumber,
			int PageSize
		)
		{
			Expression<Func<Animal, bool>> filter = a => true;

			if(!string.IsNullOrEmpty(LifeNumber))
			{
				Expression<Func<Animal, bool>> expression = a => a.LifeNumber == LifeNumber;
				filter = Util.CombineExpressionsWithAndAlso(filter, expression);
			}

			if (!string.IsNullOrEmpty(Name))
			{
				Expression<Func<Animal, bool>> expression = a => a.Name == Name;
				filter = Util.CombineExpressionsWithAndAlso(filter, expression);
			}

			if (Gender != null)
			{
				Expression<Func<Animal, bool>> expression = a => a.Gender == Gender;
				filter = Util.CombineExpressionsWithAndAlso(filter, expression);
			}

			if (BirthDate != null)
			{
				Expression<Func<Animal, bool>> expression = a => a.BirthDate == BirthDate;
				filter = Util.CombineExpressionsWithAndAlso(filter, expression);
			}

			if (!string.IsNullOrEmpty(FatherLifeNumber))
			{
				Expression<Func<Animal, bool>> expression = a => a.FatherLifeNumber == FatherLifeNumber;
				filter = Util.CombineExpressionsWithAndAlso(filter, expression);
			}

			if (!string.IsNullOrEmpty(MotherLifeNumber))
			{
				Expression<Func<Animal, bool>> expression = a => a.MotherLifeNumber == MotherLifeNumber;
				filter = Util.CombineExpressionsWithAndAlso(filter, expression);
			}

			if (!string.IsNullOrEmpty(Description))
			{
				Expression<Func<Animal, bool>> expression = a => a.Description == Description;
				filter = Util.CombineExpressionsWithAndAlso(filter, expression);
			}

			return await this._animalRepository.Get(filter, q => q.OrderBy(a => a.BirthDate), pageSize: PageSize, page: PageNumber);
		}

		public async Task<Animal?> GetAnimal(Guid id) => await this._animalRepository.GetByID(id);

		public async Task<Animal> InsertAnimal(Animal animal) => await this._animalRepository.Insert(animal);

		public async Task<Animal> UpdateAnimal(Guid id, Animal animal)
		{
			animal.Id = id;
			return await this._animalRepository.Update(animal);
		}

		public void DeleteAnimal(Guid id) => this._animalRepository.Delete(id);
	}
}
