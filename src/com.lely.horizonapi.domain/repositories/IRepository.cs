﻿using System.Linq.Expressions;

namespace com.lely.horizonapi.domain.repositories
{
	public interface IRepository<Entity> where Entity : class
	{
		/// <summary>
		/// Saves all changes made in the context to the database.
		/// </summary>
		/// <returns> 
		/// A task that represents the asynchronous save operation. The task 
		/// result contains the number of state entries written to the database.
		/// </returns>
		Task<int> Save();

		/// <summary>
		/// Queries the database based on the given inputs and returns the results. 
		/// </summary>
		/// <param name="filter">The Filters To Apply To The Query</param>
		/// <param name="orderBy">The Function That Is Used To Order The Results</param>
		/// <param name="includeProperties">Used If The Result Cunsists Of More Than One Entities</param>
		/// <param name="pageSize">The Size Of Each Page</param>
		/// <param name="page">Current Page</param>
		/// <returns>A task that represents the asynchronous query operation. The result of the task contains the result of the query.</returns>
		Task<IEnumerable<Entity>> Get(
			Expression<Func<Entity, bool>>? filter = null,
			Func<IQueryable<Entity>, IOrderedQueryable<Entity>>? orderBy = null,
			string includeProperties = "",
			int pageSize = 50,
			int page = 1
		);

		/// <summary>
		/// Queries the database based on <paramref name="id"/> and returns the result.
		/// </summary>
		/// <param name="id">The ID Of The Entity</param>
		/// <returns>A task that represents the asynchronous query operation. The result of the task contains the entity with the given id.</returns>
		Task<Entity?> GetByID(object id);

		/// <summary>
		/// Inserts <paramref name="entity"/> into the database.
		/// </summary>
		/// <param name="entity"></param>
		/// <returns>The Inserted Entity</returns>
		Task<Entity> Insert(Entity entity);

		/// <summary>
		/// Deletes the entity with the identifier <paramref name="id"/> from the database.
		/// </summary>
		/// <param name="id">The Identifier Of The Entity To Remove</param>
		void Delete(object id);

		/// <summary>
		/// Deletes <paramref name="entityToDelete"/> from the database.
		/// </summary>
		/// <param name="entityToDelete">Entity To Remove</param>
		void Delete(Entity entityToDelete);

		/// <summary>
		/// Changes the entity with the id of <paramref name="entityToUpdate"/>'s id, with <paramref name="entityToUpdate"/> in the database.
		/// </summary>
		/// <param name="entityToUpdate">Entities New Form</param>
		/// <returns>Updated Entity</returns>
		Task<Entity> Update(Entity entityToUpdate);
	}
}
