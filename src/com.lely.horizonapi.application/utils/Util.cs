﻿using System.Linq.Expressions;

namespace com.lely.horizonapi.application.utils
{
	internal class Util
	{
		/// <summary>
		/// Combines given expressions with AndAlso.
		/// </summary>
		/// <param name="expression1">First Expression</param>
		/// <param name="expression2">Second Expression</param>
		/// <returns>expression1 AndAlso expression2</returns>
		internal static Expression<Func<T, bool>> CombineExpressionsWithAndAlso<T>(Expression<Func<T, bool>> expression1, Expression<Func<T, bool>> expression2)
		{
			var invokedExpr = Expression.Invoke(expression2, expression1.Parameters.Cast<Expression>());
			return Expression.Lambda<Func<T, bool>>(Expression.AndAlso(expression1.Body, invokedExpr), expression1.Parameters);
		}
	}
}
