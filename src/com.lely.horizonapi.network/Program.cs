using com.lely.horizonapi.application;
using com.lely.horizonapi.application.services;
using com.lely.horizonapi.application.services.Interfaces;
using com.lely.horizonapi.infrastructure;
using com.lely.horizonapi.network.middlewares;
using Microsoft.OpenApi.Models;
using System.Reflection;
using Serilog;

Log.Logger = new LoggerConfiguration()
    .WriteTo.Console()
    .WriteTo.Seq("http://seq:5341")
    .CreateBootstrapLogger();

Log.Information("Starting up");

try
{
    var builder = WebApplication.CreateBuilder(args);

    builder.Host.UseSerilog((ctx, lc) => lc
        .WriteTo.Console()
        .WriteTo.Seq("http://seq:5341")
        .ReadFrom.Configuration(ctx.Configuration));

    // Add services to the container.
    builder.Services.AddInfrastructure();
    builder.Services.AddApplication();
    builder.Services.AddControllers();
    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v1", new OpenApiInfo
        {
            Title = "Horizon API",
            Version = "v1",
            Description = "A RESTful API which is for handling animal records",
            Contact = new OpenApiContact
            {
                Name = "Pouya Yavari",
                Email = "pouyayavari73@gmail.com",
            },
        });

        var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
        Console.WriteLine(xmlFile);
        var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
        Console.WriteLine(xmlPath);
        c.IncludeXmlComments(xmlPath);
    });

    builder.Services.AddScoped<IAnimalsService, AnimalsService>();
    builder.Services.AddScoped<IVisitsService, VisitsService>();

    var app = builder.Build();

    app.UseSerilogRequestLogging();

	app.UseMiddleware<ErrorHandlerMiddleware>();

    // Configure the HTTP request pipeline.
    if (app.Environment.IsDevelopment())
    {
	    app.UseSwagger();
	    app.UseSwaggerUI();
    }

    app.UseAuthorization();

    app.MapControllers();

    app.Run();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Unhandled exception");
}
finally
{
    Log.Information("Shut down complete");
    Log.CloseAndFlush();
}
