﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace com.lely.horizonapi.domain.entities
{
	[Table("Visits")]
	public class Visit
	{
		/// <summary>
		/// Visits Identifier (ambiguous!)
		/// </summary>
		[Key]
		[Column("DEVICE_ID")]
		[JsonPropertyName("DeviceId")]
		public Guid DeviceId { get; set; }

		/// <summary>
		/// Visited Animals Identifier
		/// </summary>
		[Column("ANIMAL_ID")]
		[JsonPropertyName("AnimalId")]
		public Guid AnimalId { get; set; }

		/// <summary>
		/// Date And Time Of The Visit
		/// </summary>
		[Column("FEEDING_DATE")]
		[JsonPropertyName("FeedingDate")]
		public DateTime FeedingDate { get; set; }

		/// <summary>
		/// Amount Of The Feed Consumed By The Animal (ambiguous!)
		/// </summary>
		[Column("INTAKE")]
		[JsonPropertyName("Intake")]
		public int? Intake { get; set; }
	}
}
