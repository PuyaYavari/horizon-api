﻿using com.lely.horizonapi.domain.entities;
using com.lely.horizonapi.domain.repositories;
using com.lely.horizonapi.infrastructure.repositories;
using Microsoft.Extensions.DependencyInjection;

namespace com.lely.horizonapi.application
{
	public static class DependencyInjection
	{
		public static IServiceCollection AddApplication(this IServiceCollection services)
		{
			services.AddScoped<IRepository<Animal>, BaseRepository<Animal>>();
			services.AddScoped<IRepository<Visit>, BaseRepository<Visit>>();
			return services;
		}
	}
}
