﻿using Microsoft.Extensions.DependencyInjection;

namespace com.lely.horizonapi.infrastructure
{
	public static class DependencyInjection
	{
		public static IServiceCollection AddInfrastructure(this IServiceCollection services)
		{
			services.AddDbContext<HorizonContext>();
			return services;
		}
	}
}
