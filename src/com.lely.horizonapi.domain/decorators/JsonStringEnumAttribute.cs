﻿using System.Text.Json.Serialization;

namespace com.lely.horizonapi.domain.decorators
{
	/// <summary>
	/// The decorator to serialize enums as string.
	/// </summary>
	class JsonStringEnumAttribute : JsonConverterAttribute
	{
		public JsonStringEnumAttribute() : base(typeof(JsonStringEnumConverter))
		{

		}
	}
}
