﻿using com.lely.horizonapi.application.DTOs;
using com.lely.horizonapi.application.services.Interfaces;
using com.lely.horizonapi.domain.entities;
using Microsoft.AspNetCore.Mvc;

namespace com.lely.horizonapi.network.controllers
{
	[Route("api/feeding/[controller]")]
	[ApiController]
	public class VisitsController : ControllerBase
	{
		private readonly IVisitsService _visitsService;

		public VisitsController(IVisitsService visitsService)
		{
			this._visitsService = visitsService;
		}

		/// <summary>
		/// Get all the visits with the specified criteria, page by page.
		/// </summary>
		/// <param name="AnimalId">Visited Animals Id</param>
		/// <param name="FeedingDate">The Date And Time Of The Visit</param>
		/// <param name="Intake">The Amount Of The Feed (ambiguous!)</param>
		/// <param name="PageNumber">Page To Get</param>
		/// <param name="PageSize">The Number Of Visits In Each Page</param>
		/// <returns>The visits with the given criteria.</returns>
		/// <response code="200">Visits with the given criteria</response>
		/// <response code="400">PageNumber less than 1 or PageSize larger than 100</response>
		[HttpGet]
		[ProducesResponseType(typeof(IEnumerable<VisitDTO>), 200)]
		[ProducesResponseType(typeof(ErrorDTO), 400)]
		[ProducesResponseType(500)]
		public async Task<object> GetAll(
			[FromQuery] Guid? AnimalId,
			[FromQuery] DateTime? FeedingDate,
			[FromQuery] int? Intake,
			[FromQuery] int PageNumber = 1,
			[FromQuery] int PageSize = 50
		)
		{
			if (PageNumber < 1)
				return BadRequest("PageNumber cannot be smaller than 1.");

			if (PageSize > 100)
				return BadRequest("PageSize cannot be larger than 100.");

			return await this._visitsService.GetVisits(
				AnimalId,
				FeedingDate,
				Intake,
				PageNumber,
				PageSize
			);
		}

		/// <summary>
		/// Get the visit with the given <paramref name="id"/>.
		/// </summary>
		/// <param name="id">Identifier Of The Visit To Get</param>
		/// <returns>The visit if the visit with the given id exists.</returns>
		/// <response code="200">Visit with the given id</response>
		/// <response code="404">Visit with the given id does not exist</response>
		[HttpGet("{id}")]
		[ProducesResponseType(typeof(VisitDTO), 200)]
		[ProducesResponseType(typeof(ErrorDTO), 404)]
		[ProducesResponseType(500)]
		public async Task<object> Get([FromRoute] Guid id)
		{ 
			VisitDTO? visit = await this._visitsService.GetVisit(id);
			if (visit != null)
				return visit;
			else
				return NotFound(new ErrorDTO($"Visit with given id is not found. Id: {id}"));
		}

		/// <summary>
		/// Inserts <paramref name="visit"/> into the databse.
		/// </summary>
		/// <param name="visit">Visit To Insert</param>
		/// <returns>Inserted Visit</returns>
		/// <response code="200">Inserted Visit</response>
		[HttpPost]
		[ProducesResponseType(typeof(VisitDTO), 200)]
		[ProducesResponseType(500)]
		public async Task<VisitDTO> Insert([FromBody] Visit visit)
		{
			return await this._visitsService.InsertVisit(visit);
		}

		/// <summary>
		/// Updates visit with identifier <paramref name="id"/>, based on <paramref name="visit"/>.
		/// </summary>
		/// <param name="id">Identifier Of The Visit To Update</param>
		/// <param name="visit">Visits New Value</param>
		/// <returns>Updated Visit</returns>
		/// <response code="200">Updated Visit</response>
		[HttpPut("{id}")]
		[ProducesResponseType(typeof(VisitDTO), 200)]
		[ProducesResponseType(500)]
		public async Task<VisitDTO> Update([FromRoute] Guid id, [FromBody] Visit visit)
		{
			return await this._visitsService.UpdateVisit(id, visit);
		}

		/// <summary>
		/// Deletes the visit with the identifier <paramref name="id"/> from the database.
		/// </summary>
		/// <param name="id">Identifier Of The Visit To Delete</param>
		[HttpDelete("{id}")]
		[ProducesResponseType(204)]
		public IActionResult Delete([FromRoute] Guid id)
		{
			this._visitsService.DeleteVisit(id);
			return NoContent();
		}
	}
}
