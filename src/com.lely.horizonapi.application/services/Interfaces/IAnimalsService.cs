﻿using com.lely.horizonapi.domain.entities;
using com.lely.horizonapi.domain.enums;

namespace com.lely.horizonapi.application.services.Interfaces
{
	public interface IAnimalsService
	{
		/// <summary>
		/// The function to get all the animals with the given criteria. The result is split into pages.
		/// </summary>
		/// <param name="LifeNumber">The LifeNumber Of The Animals</param>
		/// <param name="Name">The Name Of The Animals</param>
		/// <param name="Gender">The Gender Of The Animals</param>
		/// <param name="BirthDate">The BirthDate Of The Animals</param>
		/// <param name="FatherLifeNumber">The FatherLifeNumber Of The Animals</param>
		/// <param name="MotherLifeNumber">The MotherLifeNumber Of The Animals</param>
		/// <param name="Description">The Description Of The Animals</param>
		/// <param name="PageNumber">Page To Get</param>
		/// <param name="PageSize">Size Of The Page To Get</param>
		/// <returns>A task that represents the asyncronus get operation. The task result contains the animals with the given criteria.</returns>
		public Task<IEnumerable<Animal>> GetAnimals(
			string? LifeNumber,
			string? Name,
			AnimalGender? Gender,
			DateTime? BirthDate,
			string? FatherLifeNumber,
			string? MotherLifeNumber,
			string? Description,
			int PageNumber,
			int PageSize
		);

		/// <summary>
		/// The function to get the animal with the given <paramref name="id"/>.
		/// </summary>
		/// <param name="id">Identifier Of The Animal To Get</param>
		/// <returns>
		/// A task that represents the asyncronus get operation. The task result contains the animal if the visit with the given id exists, else null.
		/// </returns>
		public Task<Animal?> GetAnimal(Guid id);

		/// <summary>
		/// Inserts <paramref name="animal"/> into the databse.
		/// </summary>
		/// <param name="animal">Animal To Insert</param>
		/// <returns>
		/// A task that represents the asyncronus insert operation. The task result contains the inserted animal.
		/// </returns>
		public Task<Animal> InsertAnimal(Animal animal);

		/// <summary>
		/// Updates animal with identifier <paramref name="id"/>, based on <paramref name="animal"/>.
		/// </summary>
		/// <param name="id">Identifier Of The Animal To Update</param>
		/// <param name="animal">Animals New Value</param>
		/// <returns>
		/// A task that represents the asyncronus update operation. The task result contains the updated animal.
		/// </returns>
		public Task<Animal> UpdateAnimal(Guid id, Animal animal);

		/// <summary>
		/// Deletes the animal with the identifier <paramref name="id"/> from the database.
		/// </summary>
		/// <param name="id">Identifier Of The Animal To Delete</param>
		public void DeleteAnimal(Guid id);
	}
}
