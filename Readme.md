# Horizon API

## Introduction
A RESTful API which is for handling animal 
records.

## Technologies
Main technologies used in this project is as below.

### [.NET 6](https://dotnet.microsoft.com/en-us/download/dotnet/6.0)
The project is developed using .Net 6 and C#.

### [SQLite](https://www.sqlite.org/index.html)
Data is stored in a SQLite database.

### [Entity Framework Core](https://docs.microsoft.com/en-us/ef/core/)
DB communication is done using EF Core 6.

### [Docker](https://www.docker.com/)
The application consists of two docker containers. One is the main container which contains the application, other contains Seq and is used for log gathering and monitoring. These containers are orchestrated using docker composer.

### [Swagger](https://swagger.io/)
The api is documented using swagger.

### [Serilog](https://serilog.net/)
The logging of the application is done using Serilog.

### [Datalust SEQ](https://datalust.co/)
Used to gather and analyze logs.

### [xUnit](https://xunit.net/)
The main testing framework.

### [Moq](https://github.com/moq/moq4)
Used for mocking certain functions or behaviours during tests.

### [Bogus](https://github.com/bchavez/Bogus)
Used for creating mock data for tests.

## Architecture
The architecture consists of four main, inner layers cannot access outer layers. These layers are:

### Domain
This is the innermost layer. This layer contains entities, enums and main interfaces.

### Infrastructure
This is the layer above domain. This layer contains the database context and the repositories.

### Application
This is the layer above infrastructure. This is where the main business logic takes place.

### Network
This is the outer-most layer of the application. This layer contains the controllers and the networking logic of the application.

### Test
Tests are above all of these layers and can access all of them.

</br></br>

<p align="center">
    <img src="doc/sketches/Architecture.svg" alt="Architecture" width="80%;"/>
</p>

### Deployment

You need Docker with Linux containers backend to be able to run the application.

Make sure docker is running, you have an internet connection and your 5000 and 88 ports are free.

Open terminal navigate to projects `src` directory and do `docker-compose up`.

You shoud be able to access Swagger from `http://localhost:5000/swagger/index.html` and Seq from `http://localhost:88/`.

You can also use `doc/Horizon API.postman_collection.json`.