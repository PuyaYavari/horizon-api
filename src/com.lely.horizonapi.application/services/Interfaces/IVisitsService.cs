﻿using com.lely.horizonapi.application.DTOs;
using com.lely.horizonapi.domain.entities;

namespace com.lely.horizonapi.application.services.Interfaces
{
	public interface IVisitsService
	{
		/// <summary>
		/// The function to get all the visits with the given criteria. The result is split into pages.
		/// </summary>
		/// <param name="AnimalId">The Id Of The Visited Animal</param>
		/// <param name="FeedingDate">The Date And Time Of The Visit</param>
		/// <param name="Intake">The Amount Of The Feed (ambiguous!)</param>
		/// <param name="PageNumber">Page To Get</param>
		/// <param name="PageSize">Size Of The Page To Get</param>
		/// <returns>A task that represents the asyncronus get operation. The task result contains the visits with the given criteria.</returns>
		public Task<IEnumerable<VisitDTO>> GetVisits(
			Guid? AnimalId,
			DateTime? FeedingDate,
			int? Intake,
			int PageNumber,
			int PageSize
		);

		/// <summary>
		/// The function to get the visit with the given <paramref name="id"/>.
		/// </summary>
		/// <param name="id">Identifier Of The Visit To Get</param>
		/// <returns>
		/// A task that represents the asyncronus get operation. The task result contains the visit if the visit with the given id exists, else null.
		/// </returns>
		public Task<VisitDTO?> GetVisit(Guid id);

		/// <summary>
		/// Inserts <paramref name="visit"/> into the databse.
		/// </summary>
		/// <param name="visit">Visit To Insert</param>
		/// <returns>
		/// A task that represents the asyncronus insert operation. The task result contains the inserted visit.
		/// </returns>
		public Task<VisitDTO> InsertVisit(Visit visit);

		/// <summary>
		/// Updates visit with identifier <paramref name="id"/>, based on <paramref name="visit"/>.
		/// </summary>
		/// <param name="id">Identifier Of The Visit To Update</param>
		/// <param name="visit">Visits New Value</param>
		/// <returns>
		/// A task that represents the asyncronus update operation. The task result contains the updated visit.
		/// </returns>
		public Task<VisitDTO> UpdateVisit(Guid id, Visit visit);

		/// <summary>
		/// Deletes the visit with the identifier <paramref name="id"/> from the database.
		/// </summary>
		/// <param name="id">Identifier Of The Visit To Delete</param>
		public void DeleteVisit(Guid id);
	}
}
